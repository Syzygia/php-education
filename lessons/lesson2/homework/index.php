<?php
require_once 'Controllers/Controller.php';
require_once 'Controllers/UserController.php';
require_once 'Controllers/PostController.php';
require_once 'Controllers/CommentController.php';

use controllers\{UserController, PostController, CommentController};

function check_uri ($path, int &$id)
{
		if (sizeof($path) >= 3)
		{
			$id = (int) ($path[2]);
		}
}

$path = explode ('/', $_SERVER["REQUEST_URI"]);
$userController = new UserController();
$postController = new PostController();
$commentController = new CommentController();
$id = -1;
check_uri($path, $id);
switch ($path[1])
{
	case 'users':
	{
		if (sizeof($path) >=3)
		{
			if ($path[2] == 'create')
			{
				$userController->create();
				break;
			}
		}
		$userController->get($id);
		break;
	}
	case 'posts':
	{
		if (sizeof($path) == 3)
		{
			$postController->create();
		}
		if (sizeof($path) >= 4)
		{
			$id = (int) ($path[2]);
			$postController->add_comment($id);
			$postController->get();
			break;
		}
		$postController->get($id);
		break;
	}
	default:
	{
		if (strpos($path[1], 'comments') !== false)
		{
			$commentController->add_options($path[1]);
			$commentController->get();
		}
		break;
	}
}


#print_r(explode('/', '/users/1/posts'));

<?php


namespace controllers;


class PostController extends Controller
{
	public string $url = 'https://jsonplaceholder.typicode.com/posts';
	protected string  $local_url = 'public/uploads/post.json';

	public function add_comment($id)
	{
		$this->url .="/$id/comments";
	}
	public function create()
	{
		$this->getLocal();
		$object = [];
		$object['id'] = (int)end($this->entities)->id + 1;
		$object['userId'] = rand(1, 10);
		$object['title'] = $this->generateRandomString(5);
		$object['body'] = $this->generateRandomString();
		$this->entities[] = $object;
		file_put_contents($this->local_url, json_encode($this->entities));
		echo 'new Post was successfully created';
	}
}
<?php

namespace controllers;

abstract class Controller
{
	protected string $url;
	protected string $local_url;
	protected array $entities = [];

	public function get (int $id = -1)
	{
		if ($id != -1)
		{
			$this->url .= "/$id";
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->url);
		$data = curl_exec($ch);
		curl_close($ch);
		$response = is_string($data) ? json_decode($data) : $data;
		echo '<pre>'.print_r($response).'</pre><br>';
		//echo '<pre>'.$this->getLocal().'</pre><br>';
		return $response;

	}
	protected function getLocal()
	{
		$str = file_get_contents($this->local_url);
		$this->entities = json_decode($str);
		return $str;
	}
	protected static function generateRandomString($length = 10)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	abstract public function create();
}
<?php

namespace controllers;

class UserController extends Controller
{
	public string $url = 'https://jsonplaceholder.typicode.com/users';
	protected string  $local_url = 'public/uploads/users.json';

	public function create()
	{
		//$this->getLocal();
		$str = $this->get(rand(1, 10));
		$this->entities[] = $str;
		file_put_contents($this->local_url, json_encode($this->entities, JSON_PRETTY_PRINT));
	}
}
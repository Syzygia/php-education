<?php

require_once "Core/Model.php";
require_once "Core/User.php";
require_once "Core/Monster.php";

use Core\{User, Monster};

function debug($var)
{
    echo "<pre>".print_r($var, true)."</pre><br>";
}

$user = new User();

debug($user);

$user->setName('Данила');
$user->setPassword('password');

debug($user);

$monster = new Monster();
$monster->setType('some type...');

debug($monster);

$monster->setType(Monster::SHOWMAN_TYPE);

debug($monster);

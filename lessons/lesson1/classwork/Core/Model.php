<?php

namespace Core;

class Model
{
    protected $name;

    public function __construct(string $name = 'Аноним')
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }
}
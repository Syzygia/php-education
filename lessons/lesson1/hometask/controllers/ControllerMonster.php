<?php


namespace controllers;


use Core\Monster;

class ControllerMonster
{
	private $monsters = [];
	public function __construct($monsters)
	{
		$this->monsters = $monsters;
	}

	public function getMonsters()
	{
		foreach ($this->monsters as $monster){
			echo $monster->getType()."<br>";
		}
	}
}
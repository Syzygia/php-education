<?php

require_once "Core/Model.php";
require_once "Core/User.php";
require_once "Core/Monster.php";
require_once "controllers/ControllerMonster.php";
require_once "controllers/ControllerUser.php";

use Core\{User, Monster};
use controllers\{ControllerMonster, ControllerUser};

$monster = new Monster();
$monster->setType(Monster::SHOWMAN_TYPE);
$monster1 = new Monster();
$monster1->setType(Monster::VAMPIRE_TYPE);
$controllers = [
	'Monsters' => new ControllerMonster([$monster, $monster1])
];
if (strpos( $_SERVER["REQUEST_URI"], 'Monster') != false)
{
    $controllers['Monsters']->getMonsters();
}

$user = new User('Даня');
$user1 = new User('Аня');
$users = [$user, $user1];
$controllers['Users'] = new ControllerUser($users);
if (strpos( $_SERVER["REQUEST_URI"], 'User') != false)
{
	$controllers['Users']->getUsers();
}
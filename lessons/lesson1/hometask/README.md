#Домашняя работа #1

__Описание работы:__

Нужно написать обработчик строки запроса которая будет идти после `http://localhost/php-education/lessons/lesson1/hometask/`

Для удобства работы можешь создать в директории `xampp/htdocs/` файл `.htaccess` со следующим содержанием:

```
AddDefaultCharset utf-8
RewriteEngine On
RewriteRule ^(.*)$ /hometask/$1
```

И скопировать данную папку под названием `hometask`, изменив внутренний файл `.htaccess` на следующее:

```
RewriteCond %(REQUEST_FILENAME) !-f
RewriteCond %(REQUEST_FILENAME) !-d
RewriteRule (.*) index.php?$1 [L,QSA]
```

Тогда ты сможешь работать с маршрутом `http://localhost`

______________________________

P.S.: Не заморачивайся по поводу файлов `.htaccess` - они необходимы для корректной работы Apache, 
далее мы откажемся от стека XAMPP и будем использовать NGINX. 

_________________________________

Полезные функции и глобальные переменные:
- rtrim
- strlen
- substr
- $_SERVER, в частности, $_SERVER["REQUEST_URI"]
- preg_match
- is_string
- isset
- class_exists
- file_exists
- method_exists
- http_response_code
- str_replace
- ucwords
- lcfirst
- explode
- strpos
- is_file

Для дебага можешь объявить глобально следующую функцию и смотреть в браузере:

```
function debug($var) {
    echo '<pre>' . print_r($var, true) . '</pre>';
}
```


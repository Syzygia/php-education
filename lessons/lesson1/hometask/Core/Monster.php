<?php

namespace Core;

class Monster extends Model
{
    const VAMPIRE_TYPE = 'vampire';
    const SHOWMAN_TYPE = 'showman';
    const TYPES = [
        self::VAMPIRE_TYPE => 'Дракула',
        self::SHOWMAN_TYPE => 'Снеговик',
    ];

    private $type;

    public function setType(string $type)
    {
        if (in_array($type, array_keys(self::TYPES))) {
            $this->type = self::TYPES[$type];
        }
    }

    public function getType()
    {
        return $this->type;
    }
}
#Образовательный проект

Модуль **PHP** :
- Документация по [PHP](http://php.net)
- IDE для PHP - [PhpStorm](https://www.jetbrains.com/ru-ru/phpstorm/)
- Книга по PHP7 - [PHP7 в подлиниках](https://yadi.sk/i/Vlzyo3_6yssWSw)
- Набор полезных [hot keys](https://hotkeysworld.com/ru/phpstorm) для PhpStorm

